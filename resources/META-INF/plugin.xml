<?xml version="1.0" encoding="iso-8859-1"?>

<!--
  ~ This file is part of "hybris integration" plugin for Intellij IDEA.
  ~ Copyright (C) 2014-2016 Alexander Bartash <AlexanderBartash@gmail.com>
  ~
  ~ This program is free software: you can redistribute it and/or modify
  ~ it under the terms of the GNU Lesser General Public License as
  ~ published by the Free Software Foundation, either version 3 of the
  ~ License, or (at your option) any later version.
  ~
  ~ This program is distributed in the hope that it will be useful,
  ~ but WITHOUT ANY WARRANTY; without even the implied warranty of
  ~ MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
  ~ See the GNU Lesser General Public License for more details.
  ~
  ~ You should have received a copy of the GNU Lesser General Public License
  ~ along with this program. If not, see <http://www.gnu.org/licenses/>.
  -->

<idea-plugin xmlns:xi="http://www.w3.org/2001/XInclude">

    <id>ccom.intellij.idea.plugin.hybris.impex</id>
    <name>hybris integration</name>
    <version>7.0.2</version>

    <depends>com.intellij.modules.lang</depends>

    <depends optional="true" config-file="ant-support-optional-dependencies.xml">AntSupport</depends>
    <depends optional="true" config-file="junit-optional-dependencies.xml">JUnit</depends>
    <depends optional="true" config-file="maven-support-optional-dependencies.xml">org.jetbrains.idea.maven</depends>
    <depends optional="true" config-file="eclipse-support-optional-dependencies.xml">org.jetbrains.idea.eclipse</depends>
    <depends optional="true" config-file="gradle-support-optional-dependencies.xml">org.jetbrains.plugins.gradle</depends>
    <depends optional="true" config-file="lang-optional-dependencies.xml">com.intellij.modules.lang</depends>
    <depends optional="true" config-file="spring-optional-dependencies.xml">com.intellij.spring</depends>
    <depends optional="true" config-file="javaee-optional-dependencies.xml">com.intellij.javaee</depends>
    <depends optional="true" config-file="java-optional-dependencies.xml">com.intellij.modules.java</depends>
    <depends optional="true" config-file="diagram-optional-dependencies.xml">com.intellij.diagram</depends>

    <vendor email="AlexanderBartash@gmail.com"
            url="https://hybris-integration.atlassian.net/wiki/display/IIPPS/Intellij+IDEA+Plugin+Public+Space">
        Alexander Bartash
    </vendor>

    <description>
        <![CDATA[
          This plugin is an open source (LGPL) plugin for SAP Hybris Commerce integration.

          <br/>
          <br/>
          Bug tracker: <a href="https://hybris-integration.atlassian.net/projects/IIPS">Intellij IDEA Plugin Support</a>

          <br/>
          <br/>
          If you have any questions you can send an email to:
          <br/>
          <a href="mailto:AlexanderBartash@gmail.com?cc=martin.zdarsky@hybris.com">Alexander Bartash</a> (repo owner)
          <br/>
          <a href="mailto:martin.zdarsky@hybris.com?cc=AlexanderBartash@gmail.com">Martin Zdarsky-Jones</a>

          <br/>
          <br/>
          <b>Features:</b>
          <br/>
          - Import of Hybris extensions to Intellij IDEA.
          <br/>
          - Automatic configuration of Spring, Web and Ant Intellij IDEA plugins.
          <br/>
          - Custom editor for impex files with automatic formatting, find usages and go to declaration actions.
          <br/>
          - Custom editor for items.xml with validation and easy navigation.
          <br/>
          - Visualization of business process graph (use context menu of the file "Diagrams/Show Diagram", only Ultimate IDEA).
          <br/>
          - Enhanced project view tree.
          <br/>
          - Syntax highlighting for flexible search queries.

          <br/>
          <br/>
          <b>Contribution guidelines:</b>
          <br/>
          - Please read <a href="http://developercertificate.org/">Contributor License Agreement</a>
          <br/>
          - Available tasks are in our <a href="https://hybris-integration.atlassian.net/projects/IIP">JIRA</a> (requires a login but you can <a href="https://hybris-integration.atlassian.net/admin/users/sign-up">sign-up</a>) also you can suggest new features or report bugs without login on our <a href="https://hybris-integration.atlassian.net/projects/IIPS">Support Desk</a> it has anonymous access.
          <br/>
          - <a href="https://hybris-integration.atlassian.net/wiki/display/IIPPS/How+to+Configure+Project+Environment+For+Plugin+Developers">How to Configure Project Environment For Plugin Developers</a>
          <br/>
          - We are working with <a href="https://www.atlassian.com/git/tutorials/making-a-pull-request/">Pull Requests</a> because it allows us to review the code before merging it. You need to fork this repository, implement a feature in a separate branch, then send us a pull request.
          <br/>
          - Be sure to include into your pull request and all commit messages the following line: "Signed-off-by: Your Real Name your.email@email.com" otherwise it can not be accepted. Use your real name (sorry, no pseudonyms or anonymous contributions).
          <br/>
          - For additional questions you can send an email to <a href="mailto:AlexanderBartash@gmail.com?cc=martin.zdarsky@hybris.com">Alexander Bartash</a> or <a href="mailto:martin.zdarsky@hybris.com?cc=AlexanderBartash@gmail.com">Martin Zdarsky-Jones</a>

          <br/>
          <br/>
          By installing thing plugin you agree to sending us anonymous statistics about plugin usage. We do not collect any information about you or your project. We just want to know which features from this plugin our users need most and learn new ways to make thing plugin better.

          <br/>
          <br/>
          <b>Developers:</b>
          <br/>
          - Alexander Bartash
          <br/>
          - Vlad Bozhenok
          <br/>
          - Martin Zdarsky-Jones
          <br/>
          - Alexander Nosov
          <br/>
          - Michael Golubev
          <br/>
          - Eugene Kudelevsky
          <br/>
          - Markus Priegl
          <br/>
          - Sergei Aksenenko
          <br/>
          - Roger Ye
          <br/>
          - Hector Longarte
      ]]>
    </description>

    <change-notes>
        <![CDATA[
        <h2> Fault </h2>
        <ul>
            <li>
                [<a href='https://hybris-integration.atlassian.net/browse/IIPS-60'>IIPS-60</a>] - Compilation from IDEA fails with: "java.lang.ClassNotFoundException: org.apache.commons.io.FilenameUtils"
            </li>
        </ul>
        <h2> Task </h2>
        <ul>
            <li>
                [<a href='https://hybris-integration.atlassian.net/browse/IIP-284'>IIP-284</a>] - Generate &quot;backoffice&quot; and &quot;hmc&quot; JARs during compilation
            </li>
            <li>
                [<a href='https://hybris-integration.atlassian.net/browse/IIP-290'>IIP-290</a>] - Collect statistics for several events
            </li>
        </ul>
        <h2> Bug </h2>
        <ul>
            <li>
                [<a href='https://hybris-integration.atlassian.net/browse/IIP-285'>IIP-285</a>] - Add-on compilation works incorrectly if multiple modules depend on it
            </li>
            <li>
                [<a href='https://hybris-integration.atlassian.net/browse/IIP-286'>IIP-286</a>] - Test classes are copied into wrong paths during compilation
            </li>
            <li>
                [<a href='https://hybris-integration.atlassian.net/browse/IIP-293'>IIP-293</a>] - Module &#39;platform&#39; doesn&#39;t have a dependency to &#39;config&#39; module
            </li>
            <li>
                [<a href='https://hybris-integration.atlassian.net/browse/IIP-297'>IIP-297</a>] - &quot;New&quot; action: hybris-specific items shouldn&#39;t be shown in non-hybris project
            </li>
        </ul>
        ]]>
    </change-notes>

    <!-- http://www.jetbrains.org/intellij/sdk/docs/basics/getting_started/build_number_ranges.html -->
    <idea-version since-build="172"/>

    <xi:include href="/META-INF/plugin-internal.xml" xpointer="xpointer(/idea-plugin/*)"/>
    <xi:include href="/META-INF/plugin-community.xml" xpointer="xpointer(/idea-plugin/*)"/>

</idea-plugin>
